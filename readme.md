# Test instructions

## Main requirements:
- Given a words.txt file containing a newline-delimited list of dictionary words, please implement the Anagrams class,
  so that the get_anagrams() method returns all anagrams from words.txt for a given word.
- Expose the functionality of the Anagrams class via a REST API endpoint implemented using flask which accepts a word,
  and returns the anagrams for it in JSON format. To demonstrate that the application works you use the Werkzeug
  development server.
- Write tests for the Anagrams class and the API endpoint using pytest.
- Explain how you would host the API implemented above in the cloud in a scalable and fault-tolerant way. You can assume
  that common components like virtual network, load balancer, etc. are available regardless of the cloud provider. Also,
  if you deem it's suitable you can use your own custom components (enough to describe what they would do), or you can
  split up the application into multiple services.

## Bonus requirements:
  - Optimize the implementation for fast retrieval
  - Thread safe implementation

# The approach

## The anagrams
Anagrams are common interview questions. The definition of the anagram is "word, phrase, or name formed by rearranging the letters of another".

In python we can compare the sorted strings after transforming them into lowercase.

```
sorted(word_1.lower()) == sorted(word_2.lower())

# Example

Initial   dog == God
Lowercase dog == god
Sorting   dgo == dgo
Result    True
```

In a more strict sense dealing with the letters is not enough, we need to handle punctuation and whitespaces as well. For example `dormitory == dirty room` should be True, but we need to get rid of the whitespaces to get True. There are many ways to handle this. Let's call the process of cleaning, transforimg to lowercase and sorting `normalization`.

## The anagram comparison
While dealing with the task, I had a focus on optimization when it came to the `get_anagrams()` method since this is going to be the main functionality of the web app and the most frequently called. 

The anagram lookup can be real quick with picking the proper data structure. I decided on the following dictionary
```
{
  normalized_word_1 : [
    anagram_1,
    anagram_2
  ],

  normalized_word_2 : [
    anagram_3,
    anagram_4
  ],
  ...
}

# Example

anagrams = {
  "dgo" : [
    "dog"
    "God",
    "god"
  ],

  "opst": [
    "post",
    "spot",
    "stop",
    "tops"
  ],
  ...
}

```
To get all the anagrams for `user_input`:
```
anagrams[normalize_word(user_input)] -> List
```
The main reason for this structure is the lookup speed and siplicity. The second reason is updatability. This structure can be easily updated with new words, placed into the proper key:value pair.

### Pros
* quick lookup
* appendable
* simple structure

### Cons
* has to run at every startup
* only stored in memory, changes gets lost on restart (see solution later)

# Anagrams in the cloud
One solution that I highly recommend is storing the anagrams and the normalized words in a database.
A simple, one table schema could be:
```
CREATE TABLE ANAGRAMS(
   ID INT NOT NULL,
   NORMALIZED VARCHAT(255) NOT NULL,
   WORD VARCHAT(255) NOT NULL,    
   PRIMARY KEY (ID)
);
```
This can be querried as such:
```
SELECT
  WORD
FROM ANAGRAMS
WHERE
  NORMALIZED = normalized_word_input;
```
The database solution is higly recommended for cloud solutions, so  paralell processes can acces the data, changes can be made and stored properly.

When it comes to devops I don't have much experience, but here's my best answer. I'd start with one server and placing a load balancer, that triggers the start of a new machine when the number of request / active machines reaches a certain threshold.

All these machines would do the same, access the same database and return json formatted responses based on the query results.

When the average number of requests / active machine falls under a threshold an stay there for a certain amount of time, active machines may be shut down.

# Requirements
This was developed with `Python 3.10.5`, `flask 2.2.1` and `pytest 7.2.0`

# Testing
To run tests run `pytest tests.py`