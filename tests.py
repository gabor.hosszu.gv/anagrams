import pytest
import json

from anagrams import (
    Anagrams,
    normalize_word,
    app,
    CHARACTER_LIMIT
    )


@pytest.mark.parametrize(
    ("word", "exp"),
    (
        ("dog", "dgo"),
        ("Dog", "dgo"),
        ("o-dg", "dgo"),
        ("d O G", "dgo"),
        ("dog2", "2dgo"))
)
def test_noramlize_word(word, exp):
    assert normalize_word(word) == exp


class TestAnagrams:
    @pytest.mark.parametrize(
        ("word", "exp"),
        [
            ("platest", ["peltast", "spattle"]),
            ("eat", ["ate", "eat", "eta", "tae", "tea"]),
        ]
    )
    def test_anagrams(self, word, exp):
        anagrams = Anagrams()

        assert anagrams.get_anagrams(word) == exp


class TestApi:
    @pytest.mark.parametrize(
        ("word", "anagrams"),
        [
            ("platest", ["peltast", "spattle"]),
            ("eat", ["ate", "eat", "eta", "tae", "tea"]),
            ("mi6", [])
        ]
    )
    def test_anagrams_get_success(self, word, anagrams):
        response = app.test_client().get(f"/anagrams/{word}")
        exp = {"anagrams": anagrams, "input": word}

        assert response.status_code == 200
        assert json.loads(response.data.decode("utf-8")) == exp

    def test_anagrams_get_fail_character_limit(self):
        response = app.test_client().get(f"/anagrams/{'ab'*CHARACTER_LIMIT}")

        assert response.status_code == 400

    def test_anagrams_get_fail_no_input(self):
        response = app.test_client().get("/anagrams/")

        assert response.status_code == 404

    @pytest.mark.parametrize(
            "method", (
                "delete",
                "patch",
                "post",
            ),
        )
    def test_anagrams_forbiddent_methods(self, method):
        method_request = getattr(app.test_client(), method)
        response = method_request("/anagrams/test")

        assert response.status_code == 405
