from flask import Flask, Response
from typing import Dict, List
import string

PUNCTIATION = string.punctuation + " "
CHARACTER_LIMIT = 255


def normalize_word(word: str) -> str:
    # removes punctiation and whitespace
    # returns sorted lowercase chars
    word = word.lower().strip()
    for punc in PUNCTIATION:
        word = word.replace(punc, "")
    return "".join(sorted(word))


class Anagrams:
    def __init__(self) -> None:
        self.anagrams = {}
        self.generate_anagrams()

    def generate_anagrams(self):
        with open("words.txt", "r") as word_file:
            for line in word_file.readlines():
                line = line.strip()
                if line not in string.ascii_uppercase:
                    self.add_anagram(line)

    def add_anagram(self, word: str) -> None:
        word_normalized = normalize_word(word)
        if word_normalized in self.anagrams:
            self.anagrams[word_normalized].append(word)
        else:
            self.anagrams[word_normalized] = [word]

    def get_anagrams(self, word: str) -> Dict[str, List[str]]:
        return self.anagrams.get(normalize_word(word), [])


app = Flask("anagrams")
anagram_service = Anagrams()


@app.route("/anagrams/<word>", methods=["GET"])
def anagrams(word: str):
    # Validation
    if len(word) > CHARACTER_LIMIT:
        return Response(
            f"Length of input is limited to {CHARACTER_LIMIT}",
            status=400
            )

    return {
        "input": word,
        "anagrams": anagram_service.get_anagrams(word)
        }


if __name__ == "__main__":
    app.run(debug=True)
